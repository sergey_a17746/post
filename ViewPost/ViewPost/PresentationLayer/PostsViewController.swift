//
//  PostsViewController.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

protocol PostsViewControllerOutput: AnyObject {
	func readyToDisplay()
}

protocol PostsViewControllerProtocol: AnyObject {
	func updatePostTable(for items: [TableViewItem])
}

class PostsViewController: UIViewController {
	private let output: PostsViewControllerOutput
	private let tableView: TableView
	private let indicator: IndicatorView

	init(_ output: PostsViewControllerOutput) {
		self.output = output
		self.tableView = TableView()
		self.indicator = IndicatorView()
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		prepareTable()
		indicator.startAnimating()
		output.readyToDisplay()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.navigationBar.topItem?.title = "Posts"
	}

	private func prepareTable() {
		view.backgroundColor = .white
		view.addSubview(tableView)
		view.addSubview(indicator)
		NSLayoutConstraint.activate([
			indicator.topAnchor.constraint(equalTo: view.topAnchor),
			indicator.leftAnchor.constraint(equalTo: view.leftAnchor),
			indicator.bottomAnchor.constraint(equalTo: view.bottomAnchor),
			indicator.rightAnchor.constraint(equalTo: view.rightAnchor),

			tableView.topAnchor.constraint(equalTo: view.topAnchor),
			tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
			tableView.rightAnchor.constraint(equalTo: view.rightAnchor)
		])
	}
}

// MARK: - PostsViewControllerProtocol

extension PostsViewController: PostsViewControllerProtocol {
	func updatePostTable(for items: [TableViewItem]) {
		indicator.startAnimating()
		if !items.isEmpty {
			DispatchQueue.main.async {
				self.tableView.updateTableView(items)
				self.indicator.stopAnimating()
			}
		}
	}
}
