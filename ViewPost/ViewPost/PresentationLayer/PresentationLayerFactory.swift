//
//  PresentationLayerFactory.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

protocol DomainLayerFactoryProtocol {
	func makePostInteractor() -> PostInteractorProtocol
}

protocol PresentationLayerFactoryProtocol {
	func makePostFlowCoordinator() -> FlowCoordinatorProtocol
}

class PresentationLayerFactory {
	private let domainLayerFactory: DomainLayerFactoryProtocol

	init(_ domainLayerFactory: DomainLayerFactoryProtocol) {
		self.domainLayerFactory = domainLayerFactory
	}
}

// MARK: - PresentationLayerFactoryProtocol

extension PresentationLayerFactory: PresentationLayerFactoryProtocol {
	func makePostFlowCoordinator() -> FlowCoordinatorProtocol {
		let assembly = PostAssembly()
		let router = PostRouter()
		let interactor = self.domainLayerFactory.makePostInteractor()

		let coordinator = PostFlowCoordinator(router: router, interactor: interactor, assembly: assembly)
		return coordinator
	}
}
