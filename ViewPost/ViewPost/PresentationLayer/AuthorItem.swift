//
//  AuthorItem.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

struct AuthorItem {
	var title: String
	var body: String
	var name: String
	var username: String
	var email: String

	init(title: String,
		 body: String,
		 name: String?,
		 username: String?,
		 email: String?) {
		self.title = title.replacingOccurrences(of: "\n", with: "")
		self.body = body.replacingOccurrences(of: "\n", with: "")
		self.name = "Name: \(name ?? "")"
		self.username = "Username: \(username ?? "")"
		self.email =  "email: \(email ?? "")"
	}
}
