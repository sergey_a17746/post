//
//  IndicatorView.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 25/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

class IndicatorView: UIView {
	private let indicator: UIActivityIndicatorView

	override init(frame: CGRect) {
		self.indicator = UIActivityIndicatorView(style: .gray)
		super.init(frame: frame)
		backgroundColor = .init(white: 0.97, alpha: 0.8)
		alpha = 0.0
		translatesAutoresizingMaskIntoConstraints = false
		prepareIndicator()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func prepareIndicator() {
		indicator.translatesAutoresizingMaskIntoConstraints = false
		indicator.hidesWhenStopped = true
		addSubview(indicator)
		NSLayoutConstraint.activate([
			indicator.centerXAnchor.constraint(equalTo: centerXAnchor),
			indicator.centerYAnchor.constraint(equalTo: centerYAnchor)
		])
	}

	func startAnimating() {
		guard !indicator.isAnimating else { return }
		indicator.startAnimating()
		UIView.animate(withDuration: 0.3, animations: {
			self.alpha = 1.0
		})
	}

	func stopAnimating() {
		guard indicator.isAnimating else { return }
		UIView.animate(withDuration: 0.3, animations: {
			self.alpha = 0.0
		}, completion: { _ in
			self.indicator.stopAnimating()
		})
	}
}
