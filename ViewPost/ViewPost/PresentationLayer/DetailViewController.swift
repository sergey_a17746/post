//
//  DetailViewController.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

protocol DetailViewControllerOutput: AnyObject {
	func readyToDisplay()
}

protocol DetailViewControllerProtocol: AnyObject {
	func updateUserLabel(by item: AuthorItem)
	func updateCommentsTable(for items: [TableViewItem])
}

class DetailViewController: UIViewController {
	private let output: DetailViewControllerOutput
	private let authorView: AuthorView
	private let tableView: TableView
	private let indicator: IndicatorView

	init(_ output: DetailViewControllerOutput) {
		self.output = output
		self.authorView = AuthorView()
		self.tableView = TableView()
		self.indicator = IndicatorView()
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		prepareUI()
		indicator.startAnimating()
		output.readyToDisplay()
	}

	private func prepareUI() {
		view.backgroundColor = .white
		view.addSubview(authorView)
		view.addSubview(tableView)
		view.addSubview(indicator)
		NSLayoutConstraint.activate([
			authorView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
			authorView.leftAnchor.constraint(equalTo: view.leftAnchor),
			authorView.rightAnchor.constraint(equalTo: view.rightAnchor),

			tableView.topAnchor.constraint(equalTo: authorView.bottomAnchor),
			tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
			tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
			tableView.rightAnchor.constraint(equalTo: view.rightAnchor),

			indicator.topAnchor.constraint(equalTo: tableView.topAnchor),
			indicator.leftAnchor.constraint(equalTo: tableView.leftAnchor),
			indicator.bottomAnchor.constraint(equalTo: tableView.bottomAnchor),
			indicator.rightAnchor.constraint(equalTo: tableView.rightAnchor)
		])
	}
}

// MARK: - DetailViewControllerProtocol

extension DetailViewController: DetailViewControllerProtocol {
	func updateUserLabel(by item: AuthorItem) {
		self.authorView.updateLabels(by: item)
	}

	func updateCommentsTable(for items: [TableViewItem]) {
		self.tableView.updateTableView(items)
		if !items.isEmpty {
			self.indicator.stopAnimating()
		}
	}
}
