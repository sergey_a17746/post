//
//  PostAssembly.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

typealias PostBranch<P> = (controller: UIViewController, presenter: P)

protocol PostAssemblyProtocol {
	func makePostsBranch() -> PostBranch<PostsPresenterProtocol>
	func makeDetailBranch() -> PostBranch<DetailPresenterProtocol>
}

class PostAssembly: PostAssemblyProtocol {
	weak var coordinator: PostFlowCoordinator?

	func makePostsBranch() -> PostBranch<PostsPresenterProtocol> {
		guard let coordinator = self.coordinator  else { assert(false, "Coordinator must be set") }
		let presenter = PostsPresenter(coordinator)
		let controller = PostsViewController(presenter)
		presenter.controller = controller

		return (controller: controller, presenter: presenter)
	}

	func makeDetailBranch() -> PostBranch<DetailPresenterProtocol> {
		guard let coordinator = self.coordinator  else { assert(false, "Coordinator must be set") }
		let presenter = DetailPresenter(coordinator)
		let controller = DetailViewController(presenter)
		presenter.controller = controller

		return (controller: controller, presenter: presenter)
	}
}
