//
//  CommentTableViewCell.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

class CommentTableViewCell: TableViewCell {
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
		prepareUI()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func prepareUI() {
		selectionStyle = .none
		textLabel?.numberOfLines = 0
		detailTextLabel?.numberOfLines = 0
		detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 14.0)
		detailTextLabel?.textColor = .gray
	}

	func setupCellByItem(_ item: TableViewItem) {
		guard let item = item as? CommentTableViewItem else { fatalError("Must be \(type(of: self))!") }
		textLabel?.text = item.body
		detailTextLabel?.text = item.email
	}
}
