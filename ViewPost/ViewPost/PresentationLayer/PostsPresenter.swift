//
//  PostsPresenter.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

protocol PostsPresenterOutput: AnyObject {
	func postsNeededFordisplay()
	func didSelectedPost(_ selectedPost: PostDataModel)
}

protocol PostsPresenterProtocol: AnyObject {
	func requestedPosts(_ posts: [PostDataModel])
}

class PostsPresenter {
	weak var controller: PostsViewControllerProtocol?
	private var output: PostsPresenterOutput

	init(_ output: PostsPresenterOutput) {
		self.output = output
	}
}

// MARK: - PostsPresenterProtocol

extension PostsPresenter: PostsPresenterProtocol {
	func requestedPosts(_ posts: [PostDataModel]) {
		var items: [TableViewItem] = []
		for post in posts {
			var item = PostTableViewItem(post)
			item.tapClosure = {
				self.output.didSelectedPost(post)
			}
			items.append(item)
		}
		controller?.updatePostTable(for: items)
	}
}

// MARK: - PostsViewControllerOutput

extension PostsPresenter: PostsViewControllerOutput {
	func pullToRefresh () {
		output.postsNeededFordisplay()
	}

	func readyToDisplay() {
		output.postsNeededFordisplay()
	}
}
