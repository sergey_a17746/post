//
//  PostRouter.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

protocol PostRouterProtocol {
	var navigationController: UINavigationController? { get set }

	func push(_ controller: UIViewController)
}

class PostRouter: PostRouterProtocol {
	weak var navigationController: UINavigationController?

	func push(_ controller: UIViewController) {
		navigationController?.pushViewController(controller, animated: true)
	}
}
