//
//  DetailPresenter.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

protocol DetailPresenterOutput: AnyObject {
	func detailsNeededForDisplay()
}

protocol DetailPresenterProtocol: AnyObject {
	func requestedUser(_ user: UserDataModel?, for post: PostDataModel)
	func requestedComments(_ comments: [CommentDataModel])
}

class DetailPresenter {
	weak var controller: DetailViewControllerProtocol?
	private var output: DetailPresenterOutput

	init(_ output: DetailPresenterOutput) {
		self.output = output
	}
}

// MARK: - DetailPresenterProtocol

extension DetailPresenter: DetailPresenterProtocol {
	func requestedUser(_ user: UserDataModel?, for post: PostDataModel) {
		controller?.updateUserLabel(by: .init(title: post.title,
											  body: post.body,
											  name: user?.name,
											  username: user?.username,
											  email: user?.email))
	}

	func requestedComments(_ comments: [CommentDataModel]) {
		var items: [TableViewItem] = []
		for comment in comments {
			let item = CommentTableViewItem(comment)
			items.append(item)
		}
		controller?.updateCommentsTable(for: items)
	}
}

// MARK: - DetailViewControllerOutput

extension DetailPresenter: DetailViewControllerOutput {
	func readyToDisplay() {
		output.detailsNeededForDisplay()
	}
}
