//
//  CommentTableViewItem.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

struct CommentTableViewItem: TableViewItem {
	var body: String
	var email: String
	var tapClosure: TapClosure?

	init(_ model: CommentDataModel) {
		self.body = model.body.replacingOccurrences(of: "\n", with: "")
		self.email = "email: \(model.email)"
	}

	func viewClass() -> UITableViewCell.Type {
		return CommentTableViewCell.self
	}
}
