//
//  AuthorView.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

class AuthorView: UIView {
	var postView = UIStackView()
	var userInfoView = UIStackView()

	var title = UILabel()
	var body = UILabel()
	var name = UILabel()
	var username = UILabel()
	var email = UILabel()

	override init(frame: CGRect) {
		super.init(frame: .zero)
		translatesAutoresizingMaskIntoConstraints = false
		backgroundColor = .init(white: 0.97, alpha: 1.0)
		prepareUI()
	}

	required init(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func prepareUI() {
		preparePostView()
		prepareUserInfoView()
		prepareConstraints()
	}

	private func preparePostView() {
		title.numberOfLines = 0
		title.font = UIFont.boldSystemFont(ofSize: 17.0)
		body.numberOfLines = 0
		body.font = UIFont.systemFont(ofSize: 17.0)

		addSubview(postView)
		postView.translatesAutoresizingMaskIntoConstraints = false
		postView.backgroundColor = .red
		postView.addArrangedSubview(title)
		postView.addArrangedSubview(body)

		postView.axis = .vertical
		postView.distribution = .equalSpacing
		postView.alignment = .center
		postView.spacing = 20
		postView.isLayoutMarginsRelativeArrangement = true
		postView.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
	}

	private func prepareUserInfoView() {
		name.font = UIFont.boldSystemFont(ofSize: 14.0)
		name.textColor = .gray
		username.font = UIFont.boldSystemFont(ofSize: 14.0)
		username.textColor = .gray
		email.font = UIFont.boldSystemFont(ofSize: 14.0)
		email.textColor = .gray

		addSubview(userInfoView)
		userInfoView.translatesAutoresizingMaskIntoConstraints = false
		userInfoView.backgroundColor = .black
		userInfoView.addArrangedSubview(name)
		userInfoView.addArrangedSubview(username)
		userInfoView.addArrangedSubview(email)

		userInfoView.axis = .vertical
		userInfoView.distribution = .equalSpacing
		userInfoView.alignment = .leading
		userInfoView.spacing = 5
		userInfoView.isLayoutMarginsRelativeArrangement = true
		userInfoView.layoutMargins = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 0)
	}

	private func prepareConstraints() {
		NSLayoutConstraint.activate([
			postView.topAnchor.constraint(equalTo: topAnchor),
			postView.leftAnchor.constraint(equalTo: leftAnchor),
			postView.rightAnchor.constraint(equalTo: rightAnchor),

			userInfoView.topAnchor.constraint(equalTo: postView.bottomAnchor),
			userInfoView.leftAnchor.constraint(equalTo: leftAnchor),
			userInfoView.rightAnchor.constraint(equalTo: rightAnchor),
			userInfoView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5)
		])
	}

	func updateLabels(by item: AuthorItem) {
		self.title.text = item.title
		self.body.text = item.body
		self.name.text = item.name
		self.username.text = item.username
		self.email.text = item.email
		self.layoutIfNeeded()
	}
}
