//
//  PostFlowCoordinator.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

protocol FlowCoordinatorProtocol: AnyObject {
	var window: UIWindow? { get set }

	func start()
}

class PostFlowCoordinator: FlowCoordinatorProtocol {
	weak var window: UIWindow?
	weak var postsPresenter: PostsPresenterProtocol?
	weak var detailPresenter: DetailPresenterProtocol?

	private var selectedPost: PostDataModel?
	private var assembly: PostAssemblyProtocol
	private var router: PostRouterProtocol
	private var interactor: PostInteractorProtocol

	init(router: PostRouterProtocol, interactor: PostInteractorProtocol, assembly: PostAssembly) {
		self.router = router
		self.interactor = interactor
		self.assembly = assembly

		self.interactor.output = self
		assembly.coordinator = self
	}

	func start() {
		let controller = UINavigationController()
		window?.rootViewController = controller
		window?.makeKeyAndVisible()
		router.navigationController = controller

		let postsBranch = assembly.makePostsBranch()
		postsPresenter = postsBranch.presenter
		router.push(postsBranch.controller)
	}
}

// MARK: - DetailPresenterOutput

extension PostFlowCoordinator: DetailPresenterOutput {
	func detailsNeededForDisplay() {
		guard let selectedPost = self.selectedPost else { return }
		let selectedIDs = IDsModel(userId: selectedPost.userId, postId: selectedPost.postId)
		self.interactor.detailsNeeded(for: selectedIDs)
	}
}

// MARK: - PostsPresenterOutput

extension PostFlowCoordinator: PostsPresenterOutput {
	func didSelectedPost(_ selectedPost: PostDataModel) {
		self.selectedPost = selectedPost
		let detailBranch = assembly.makeDetailBranch()
		detailPresenter = detailBranch.presenter
		detailPresenter?.requestedUser(nil, for: selectedPost)
		router.push(detailBranch.controller)
	}

	func postsNeededFordisplay() {
		self.interactor.postsNeeded()
	}
}

// MARK: - PostInteractorOutput

extension PostFlowCoordinator: PostInteractorOutput {
	func detailUser(_ user: UserDataModel) {
		guard let selectedPost = self.selectedPost else { return }
		detailPresenter?.requestedUser(user, for: selectedPost)
	}

	func detailComments(_ comments: [CommentDataModel]) {
		detailPresenter?.requestedComments(comments)
	}

	func posts(_ posts: [PostDataModel]) {
		postsPresenter?.requestedPosts(posts)
	}
}
