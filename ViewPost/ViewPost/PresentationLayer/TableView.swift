//
//  TableView.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

typealias TableViewCell = UITableViewCell & Setupable
typealias TapClosure = () -> Void

protocol Setupable {
	func setupCellByItem(_ item: TableViewItem)
}

protocol TableViewItem {
	func viewClass() -> UITableViewCell.Type

	var tapClosure: TapClosure? { get set }
}

class TableView: UIView {
	private let identifier = "IdentifierTableViewCell_"

	private(set) var items: [TableViewItem]?
	private let tableView: UITableView

	override init(frame: CGRect) {
		self.tableView = UITableView(frame: .zero, style: .plain)
		super.init(frame: frame)
		prepareUI()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func prepareUI() {
		translatesAutoresizingMaskIntoConstraints = false

		tableView.delegate = self
		tableView.dataSource = self
		tableView.translatesAutoresizingMaskIntoConstraints = false
		self.addSubview(tableView)
		NSLayoutConstraint.activate([
			tableView.topAnchor.constraint(equalTo: self.topAnchor),
			tableView.leftAnchor.constraint(equalTo: self.leftAnchor),
			tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
			tableView.rightAnchor.constraint(equalTo: self.rightAnchor)
		])
	}

	func updateTableView(_ items: [TableViewItem]) {
		self.items = items
		var viewClass: AnyClass
		for item in items {
			viewClass = item.viewClass()
			let viewIdentifier = self.identifier + String(describing: viewClass)
			self.tableView.register(viewClass.self, forCellReuseIdentifier: viewIdentifier)
		}
		self.tableView.reloadData()
	}
}

// MARK: - UITableViewDataSource

extension TableView: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let count = items?.count else { return 0 }
		return count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let item = items?[indexPath.row] else { return UITableViewCell() }
		let cellID = identifier + String(describing: item.viewClass())
		let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)

		guard let customCell = cell as? TableViewCell else { return UITableViewCell() }
		customCell.setupCellByItem(item)
		return customCell
	}
}

// MARK: - UITableViewDelegate

extension TableView: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let item = items?[indexPath.row] else { return }
		item.tapClosure?()
	}
}
