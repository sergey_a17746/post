//
//  PostTableViewCell.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

class PostTableViewCell: TableViewCell {
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
		prepareUI()
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func prepareUI() {
		textLabel?.numberOfLines = 0
		detailTextLabel?.numberOfLines = 0
	}

	func setupCellByItem(_ item: TableViewItem) {
		guard let item = item as? PostTableViewItem else { fatalError("Must be \(type(of: self))!") }
		textLabel?.text = item.title
		detailTextLabel?.text = item.body
	}
}
