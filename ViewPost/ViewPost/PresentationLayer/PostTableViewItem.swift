//
//  PostTableViewItem.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

struct PostTableViewItem: TableViewItem {
	var title: String
	var body: String
	var tapClosure: TapClosure?

	init(_ model: PostDataModel) {
		self.title = model.title.replacingOccurrences(of: "\n", with: "")
		self.body = model.body.replacingOccurrences(of: "\n", with: "")
	}

	func viewClass() -> UITableViewCell.Type {
		return PostTableViewCell.self
	}
}
