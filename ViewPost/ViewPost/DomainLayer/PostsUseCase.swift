//
//  PostsUseCase.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

protocol PostsUseCaseProtocol {
	func requestPosts(_ type: Request, completion: @escaping UpdateCompletion<PostDataModel>)
}

class PostsUseCase {
	private let gateway: PostsGatewayProtocol

	init(gateway: PostsGatewayProtocol) {
		self.gateway = gateway
	}
}

// MARK: - PostsUseCaseProtocol

extension PostsUseCase: PostsUseCaseProtocol {
	func requestPosts(_ type: Request, completion: @escaping UpdateCompletion<PostDataModel>) {
		let posts = gateway.requestPostModels(.force, completion: completion)
		completion(posts)
	}
}
