//
//  DetailUseCase.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

protocol DetailUseCaseProtocol {
	func requestComments(for idsModel: IDsModel, completion: @escaping UpdateCompletion<CommentDataModel>)
	func requestUser(for idsModel: IDsModel, completion: @escaping UpdateCompletion<UserDataModel>)
}

class DetailUseCase {
	private let gateway: DetailGatewayProtocol

	init(gateway: DetailGatewayProtocol) {
		self.gateway = gateway
	}
}

// MARK: - DetailUseCaseProtocol

extension DetailUseCase: DetailUseCaseProtocol {
	func requestComments(for idsModel: IDsModel, completion: @escaping UpdateCompletion<CommentDataModel>) {
		self.gateway.requestCommentsModels(for: idsModel, completion: completion)
	}

	func requestUser(for idsModel: IDsModel, completion: @escaping UpdateCompletion<UserDataModel>) {
		self.gateway.requestUserModels(for: idsModel, completion: completion)
	}
}
