//
//  DomainLayerFactory.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

protocol DataLayerFactoryProtocol {
	func makePostGateway() -> PostsGatewayProtocol
	func makeDetailGateway() -> DetailGatewayProtocol
}

class DomainLayerFactory {
	private let dataLayerFactory: DataLayerFactoryProtocol

	init(_ dataLayerFactory: DataLayerFactoryProtocol) {
		self.dataLayerFactory = dataLayerFactory
	}
}

// MARK: - DomainLayerFactoryProtocol

extension DomainLayerFactory: DomainLayerFactoryProtocol {
	func makePostInteractor() -> PostInteractorProtocol {
		let posts = PostsUseCase(gateway: dataLayerFactory.makePostGateway())
		let detail = DetailUseCase(gateway: dataLayerFactory.makeDetailGateway())

		return PostInteractor(posts: posts, detail: detail)
	}
}
