//
//  PostInteractor.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

protocol PostInteractorOutput: AnyObject {
	func posts(_ posts: [PostDataModel])
	func detailUser(_ user: UserDataModel)
	func detailComments(_ comments: [CommentDataModel])
}

protocol PostInteractorProtocol {
	var output: PostInteractorOutput? { get set }

	func postsNeeded()
	func detailsNeeded(for idsModel: IDsModel)
}

class PostInteractor {
	weak var output: PostInteractorOutput?

	private let posts: PostsUseCase
	private let detail: DetailUseCase

	init(posts: PostsUseCase, detail: DetailUseCase) {
		self.posts = posts
		self.detail = detail
	}
}

// MARK: - PostInteractorProtocol

extension PostInteractor: PostInteractorProtocol {
	func postsNeeded() {
		posts.requestPosts(.force) { [weak self] posts in
			guard let self = self else { return }
			self.output?.posts(posts)
		}
	}

	func detailsNeeded(for idsModel: IDsModel) {
		detail.requestUser(for: idsModel) { [weak self] users in
			guard let self = self else { return }
			guard let user = users.first else { return }
			self.output?.detailUser(user)
		}
		detail.requestComments(for: idsModel) { [weak self] comments in
			guard let self = self else { return }
			self.output?.detailComments(comments)
		}
	}
}
