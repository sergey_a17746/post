//
//  ModuleAssembly.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import CoreData
import Foundation

struct ModuleAssembly {
	static func assemble() -> PresentationLayerFactoryProtocol {
		let container = NSPersistentContainer(name: "ViewPost")
		container.loadPersistentStores(completionHandler: { storeDescription, error in
			print("Ok! storeDescription = \(storeDescription)")
			if let error = error as NSError? {
				fatalError("Fail! Unresolved error \(error) for store description \(storeDescription), \(error.userInfo)")
			}
		})
		let serviceLayerFactory = ServiceLayerFactory(container: container) {
			URLSession(configuration: .default)
		}
		let dataLayerFactory = DataLayerFactory(serviceLayerFactory)
		let domainLayerFactory = DomainLayerFactory(dataLayerFactory)
		let presentationLayerFactory = PresentationLayerFactory(domainLayerFactory)
		return presentationLayerFactory
	}
}
