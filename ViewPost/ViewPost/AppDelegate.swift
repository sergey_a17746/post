//
//  AppDelegate.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?

	func application(_ application: UIApplication,
					 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		window = UIWindow(frame: UIScreen.main.bounds)

		let presentationLayerFactory = ModuleAssembly.assemble()
		let flowCoordinator = presentationLayerFactory.makePostFlowCoordinator()

		flowCoordinator.window = window
		flowCoordinator.start()
		return true
	}
}
