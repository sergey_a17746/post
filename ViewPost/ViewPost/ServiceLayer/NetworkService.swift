//
//  NetworkService.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

typealias Response = Result<Data?, Error>

protocol NetworkServiceProtocol {
	func asyncRequest(_ endpoint: Endpoint, completion: @escaping (Response) -> Void)
}

enum Endpoint {
	case posts
	case user(Int)
	case comments(Int)

	var value: String {
		switch self {
		case .posts:
			return "posts"
		case .user(let userId):
			return "users?id=\(userId)"
		case .comments(let postId):
			return "comments?postId=\(postId)"
		}
	}
}

class NetworkService: NSObject, NetworkServiceProtocol {
	private var session: URLSession

	init(configuration: Configuration) {
		session = configuration()
	}

	func asyncRequest(_ endpoint: Endpoint, completion: @escaping (Response) -> Void) {
		guard let url = URL(string: "https://jsonplaceholder.typicode.com/\(endpoint.value)") else { return }
		session.dataTask(with: url, completionHandler: { data, _, error in
			DispatchQueue.main.async {
				if let responseError = error { return completion(.failure(responseError)) }
				completion(.success(data))
			}
		}).resume()
	}
}
