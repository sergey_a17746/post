//
//  PostEntityModel.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

struct PostEntityModel {
	let postId: Int32
	let	userId: Int32
	let	body: String?
	let title: String?
}
