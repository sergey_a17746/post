//
//  StorageService.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import CoreData
import Foundation

protocol StorageServiceProtocol {
	func cacheEntities(_ entities: [PostEntityModel])
	func entitiesRequest() -> [PostEntityModel]
}

class StorageService: StorageServiceProtocol {
	private let container: NSPersistentContainer

	init(container: NSPersistentContainer) {
		self.container = container
	}

	func cacheEntities(_ entities: [PostEntityModel]) {
		cleanStorage(for: "PostEntity")
		let context = self.container.viewContext
		context.performAndWait {
			for postEntity in entities {
				let entity = NSEntityDescription.insertNewObject(forEntityName: "PostEntity", into: context) as? PostEntity
				entity?.body = postEntity.body
				entity?.postId = postEntity.postId
				entity?.title = postEntity.title
				entity?.userId = postEntity.userId
			}
			do {
				try context.save()
			} catch {
				print("Error! Not saved.")
			}
		}
	}

	func entitiesRequest() -> [PostEntityModel] {
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PostEntity")
		let context = self.container.viewContext
		do {
			var postEntities: [PostEntityModel] = []
			guard let entities = try context.fetch(fetchRequest) as? [PostEntity] else { return [] }
			for entity in entities {
				let postEntity = PostEntityModel(postId: entity.postId,
												 userId: entity.userId,
												 body: entity.body,
												 title: entity.title)
				postEntities.append(postEntity)
			}
			return postEntities
		} catch {
			print("Error! Not received.")
			return []
		}
	}

	func cleanStorage(for name: String) {
		let context = self.container.viewContext
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: name)
		let batchDelete = NSBatchDeleteRequest(fetchRequest: fetchRequest)
		do {
			try context.execute(batchDelete)
		} catch {
			print(error)
		}
	}
}
