//
//  ServiceLayerFactory.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import CoreData

typealias Configuration = () -> URLSession

class ServiceLayerFactory {
	let container: NSPersistentContainer
	let configuration: Configuration

	init(container: NSPersistentContainer, configuration: @escaping Configuration) {
		self.container = container
		self.configuration = configuration
	}
}

// MARK: - ServiceLayerFactoryProtocol

extension ServiceLayerFactory: ServiceLayerFactoryProtocol {
	func makeNetworkService() -> NetworkServiceProtocol {
		return NetworkService(configuration: self.configuration)
	}

	func makeStorageService() -> StorageServiceProtocol {
		return StorageService(container: container)
	}
}
