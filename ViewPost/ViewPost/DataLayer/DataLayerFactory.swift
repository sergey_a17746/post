//
//  DataLayerFactory.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

protocol ServiceLayerFactoryProtocol {
	func makeNetworkService() -> NetworkServiceProtocol
	func makeStorageService() -> StorageServiceProtocol
}

class DataLayerFactory {
	private let serviceLayerFactory: ServiceLayerFactoryProtocol

	init(_ serviceLayerFactory: ServiceLayerFactoryProtocol) {
		self.serviceLayerFactory = serviceLayerFactory
	}
}

// MARK: - DataLayerFactoryProtocol

extension DataLayerFactory: DataLayerFactoryProtocol {
	func makePostGateway() -> PostsGatewayProtocol {
		return PostsGateway(networking: serviceLayerFactory.makeNetworkService(),
							storage: serviceLayerFactory.makeStorageService(),
							mapper: ModelMapper())
	}

	func makeDetailGateway() -> DetailGatewayProtocol {
		return DetailGateway(networking: serviceLayerFactory.makeNetworkService(), mapper: ModelMapper())
	}
}
