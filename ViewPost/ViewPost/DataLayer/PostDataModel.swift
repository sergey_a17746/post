//
//  PostDataModel.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

struct PostDataModel: Codable {
	var userId: Int
	var postId: Int
	var title: String
	var body: String

	init(userId: Int, postId: Int, title: String, body: String) {
		self.userId = userId
		self.postId = postId
		self.title = title
		self.body = body
	}

	enum Keys: String, CodingKey {
		case userId = "userId"
		case postId = "id"
		case title = "title"
		case body = "body"
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: Keys.self)

		try container.encode(userId, forKey: .userId)
		try container.encode(postId, forKey: .postId)
		try container.encode(title, forKey: .title)
		try container.encode(body, forKey: .body)
	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: Keys.self)

		userId = try container.decode(Int.self, forKey: .userId)
		postId = try container.decode(Int.self, forKey: .postId)
		title = try container.decode(String.self, forKey: .title)
		body = try container.decode(String.self, forKey: .body)
	}
}
