//
//  PostsGateway.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

typealias UpdateCompletion<T> = ([T]) -> Void

enum Request {
	case normal
	case force
}

protocol PostsGatewayProtocol {
	func requestPostModels(_ type: Request, completion: @escaping UpdateCompletion<PostDataModel>) -> [PostDataModel]
}

class PostsGateway: PostsGatewayProtocol {
	private let networking: NetworkServiceProtocol
	private let storage: StorageServiceProtocol
	private let mapper: ModelMapper

	init(networking: NetworkServiceProtocol, storage: StorageServiceProtocol, mapper: ModelMapper) {
		self.networking = networking
		self.storage = storage
		self.mapper = mapper
	}

	func requestPostModels(_ type: Request, completion: @escaping UpdateCompletion<PostDataModel>) -> [PostDataModel] {
		switch type {
		case .normal:
			return normalRequestPosts()
		case .force:
			return forceRequestPosts(completion: completion)
		}
	}

	func normalRequestPosts() -> [PostDataModel] {
		let posts = mapper.mapEntities(storage.entitiesRequest())
		return posts
	}

	func forceRequestPosts(completion: @escaping UpdateCompletion<PostDataModel>) -> [PostDataModel] {
		networking.asyncRequest(.posts) { [weak self] response in
			switch response {
			case .success(let data):
				guard let self = self else { return }
				let posts = self.mapper.map(data, to: PostDataModel.self)
				self.storage.cacheEntities(self.mapper.mapPostModels(posts))
				completion(posts)
			case .failure(_):
				completion([])
			}
		}
		return normalRequestPosts()
	}
}
