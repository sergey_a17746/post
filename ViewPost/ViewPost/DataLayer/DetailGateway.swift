//
//  DetailGateway.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 23/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

protocol DetailGatewayProtocol {
	func requestCommentsModels(for idsModel: IDsModel, completion: @escaping UpdateCompletion<CommentDataModel>)
	func requestUserModels(for idsModel: IDsModel, completion: @escaping UpdateCompletion<UserDataModel>)
}

class DetailGateway: DetailGatewayProtocol {
	private let networking: NetworkServiceProtocol
	private let mapper: ModelMapper

	init(networking: NetworkServiceProtocol, mapper: ModelMapper) {
		self.networking = networking
		self.mapper = mapper
	}

	func requestCommentsModels(for idsModel: IDsModel, completion: @escaping UpdateCompletion<CommentDataModel>) {
		networking.asyncRequest(.comments(idsModel.postId)) { [weak self] response in
			guard let self = self else { return }
			self.completion(completion: completion, response: response)
		}
	}

	func requestUserModels(for idsModel: IDsModel, completion: @escaping UpdateCompletion<UserDataModel>) {
		networking.asyncRequest(.user(idsModel.userId)) { [weak self] response  in
			guard let self = self else { return }
			self.completion(completion: completion, response: response)
		}
	}

	func completion<M>(completion: UpdateCompletion<M>, response: Response) where M: Codable {
		switch response {
		case .success(let data):
			let posts = self.mapper.map(data, to: M.self)
			completion(posts)
		case .failure(_):
			completion([])
		}
	}
}
