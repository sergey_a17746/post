//
//  IDsModel.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

struct IDsModel {
	let userId: Int
	let postId: Int
}
