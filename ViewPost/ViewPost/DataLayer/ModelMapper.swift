//
//  ModelMapper.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

struct ModelMapper {
	func map<M>(_ data: Data?, to model: M.Type) -> [M] where M: Codable {
		do {
			let result: [M] = try JSONDecoder().decode([M].self, from: data ?? Data())
			return result
		} catch {
			print("Error! Mapping fail.")
			return []
		}
	}

	func mapEntities(_ entities: [PostEntityModel]?) -> [PostDataModel] {
		guard let entities = entities else { return [] }
		var posts: [PostDataModel] = []
		for entity in entities {
			let post = PostDataModel(userId: Int(entity.userId),
									 postId: Int(entity.postId),
									 title: entity.title ?? "",
									 body: entity.body ?? "")
			posts.append(post)
		}
		return posts
	}

	func mapPostModels(_ postModels: [PostDataModel]) -> [PostEntityModel] {
		var entities: [PostEntityModel] = []
		for postModel in postModels {
			let entity = PostEntityModel(postId: Int32(postModel.postId),
										 userId: Int32(postModel.userId),
										 body: postModel.body,
										 title: postModel.title)
			entities.append(entity)
		}
		return entities
	}
}
