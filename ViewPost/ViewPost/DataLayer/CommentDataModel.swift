//
//  CommentDataModel.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

struct CommentDataModel: Codable {
	var postId: Int
	var userId: Int
	var name: String
	var email: String
	var body: String

	enum Keys: String, CodingKey {
		case postId
		case userId = "id"
		case name
		case email
		case body
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: Keys.self)

		try container.encode(postId, forKey: .postId)
		try container.encode(userId, forKey: .userId)
		try container.encode(name, forKey: .name)
		try container.encode(email, forKey: .email)
		try container.encode(body, forKey: .body)
	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: Keys.self)

		postId = try container.decode(Int.self, forKey: .postId)
		userId = try container.decode(Int.self, forKey: .userId)
		name = try container.decode(String.self, forKey: .name)
		email = try container.decode(String.self, forKey: .email)
		body = try container.decode(String.self, forKey: .body)
	}
}
