//
//  UserDataModel.swift
//  ViewPost
//
//  Created by Сергей Алтухов on 24/11/2019.
//  Copyright © 2019 Сергей Алтухов. All rights reserved.
//

import Foundation

struct UserDataModel: Codable {
	var userId: Int
	var name: String
	var username: String
	var email: String

	enum Keys: String, CodingKey {
		case userId = "id"
		case name
		case username
		case email
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: Keys.self)

		try container.encode(userId, forKey: .userId)
		try container.encode(name, forKey: .name)
		try container.encode(username, forKey: .username)
		try container.encode(email, forKey: .email)
	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: Keys.self)

		userId = try container.decode(Int.self, forKey: .userId)
		name = try container.decode(String.self, forKey: .name)
		username = try container.decode(String.self, forKey: .username)
		email = try container.decode(String.self, forKey: .email)
	}
}
